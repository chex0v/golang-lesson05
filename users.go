package users

import (
	"database/sql"
	"errors"
	"fmt"
	"strings"

	_ "github.com/lib/pq"
)

type Userdata struct {
	ID          int
	Username    string
	Name        string
	Surname     string
	Description string
}

var (
	Hostname = ""
	Port     = 2345
	Username = ""
	Password = ""
	Database = ""
)

func openConnection() (*sql.DB, error) {
	conn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", Hostname, Port, Username, Password, Database)

	db, err := sql.Open("postgres", conn)
	if err != nil {
		fmt.Println("Open(): ", err)
		return nil, err
	}
	return db, nil
}

func exists(username string) int {
	username = strings.ToLower(username)

	db, err := openConnection()
	if err != nil {
		fmt.Println(err)
		return -1
	}
	defer db.Close()
	userId := -1
	statement := fmt.Sprintf(`select "id" from "users" where username='%s'`, username)
	rows, err := db.Query(statement)
	if err != nil {
		fmt.Println("Scan", err)
		return -1
	}

	for rows.Next() {
		var id int
		err = rows.Scan(&id)
		if err != nil {
			fmt.Println("Scan", err)
			return -1
		}
		userId = id
	}
	defer rows.Close()
	return userId
}

func AddUser(d Userdata) int {
	d.Username = strings.ToLower(d.Username)
	db, err := openConnection()
	if err != nil {
		fmt.Println(err)
		return -1
	}
	defer db.Close()

	userId := exists(d.Username)

	if userId != -1 {
		fmt.Println("User already exist:", d.Username)
		return -1
	}

	insertStatements := `insert into "users" ("username") values ($1)`

	_, err = db.Exec(insertStatements, d.Username)

	if err != nil {
		fmt.Println(err)
		return -1
	}

	userId = exists(d.Username)

	if userId == -1 {
		return userId
	}

	insertStatements = `insert into "usersdata" ("userid", "name", "surname", "description") values ($1, $2, $3, $4)`

	_, err = db.Exec(insertStatements, userId, d.Name, d.Surname, d.Description)

	if err != nil {
		fmt.Println("db.Exex()", err)
		return -1
	}

	return userId

}
func DeleteUser(id int) error {
	db, err := openConnection()

	if err != nil {
		return err
	}

	defer db.Close()

	statement := fmt.Sprintf(`select "username" from "users" where id=%d`, id)
	rows, err := db.Query(statement)

	if err != nil {
		return err
	}

	var username string
	for rows.Next() {
		err = rows.Scan(&username)

		if err != nil {
			return err
		}
	}
	defer rows.Close()

	if exists(username) != id {
		return fmt.Errorf("user with %d is is not exists", id)
	}

	deleteStatement := `delete from "usersdata" where userid=$1`
	_, err = db.Exec(deleteStatement, id)

	if err != nil {
		return err
	}
	deleteStatement = `delete from "users" where id=$1`
	_, err = db.Exec(deleteStatement, id)

	if err != nil {
		return err
	}
	return nil
}

func UpdateUser(d Userdata) error {
	db, err := openConnection()

	if err != nil {
		return err
	}

	defer db.Close()

	userId := exists(d.Username)

	if userId == -1 {
		return errors.New("user not exists")
	}

	updateStatement := `update "usersdata" set "name"=$1, "surname"=$2, "description"=$3 where "userid"=$4`
	_, err = db.Exec(updateStatement, d.Name, d.Surname, d.Description, d.ID)
	if err != nil {
		return err
	}

	return nil
}

func ListUsers() ([]Userdata, error) {
	Data := []Userdata{}
	db, err := openConnection()
	if err != nil {
		fmt.Println(err)
		return Data, err
	}
	defer db.Close()

	rows, err := db.Query(`select
		"id", "username", "name", "surname", "description"
		from "users", "usersdata"
		where users.id = usersdata.userid
	`)
	if err != nil {
		return Data, err
	}

	for rows.Next() {
		var id int
		var username string
		var name string
		var surname string
		var description string
		err = rows.Scan(&id, &username, &name, &surname, &description)
		temp := Userdata{ID: id, Username: username, Name: name, Surname: surname, Description: description}
		Data = append(Data, temp)
		if err != nil {
			return Data, err
		}
	}
	defer rows.Close()
	return Data, nil

}
